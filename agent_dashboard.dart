import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:national_life_insurance_agent/src/business_logics/models/user_data_model.dart';
import 'package:national_life_insurance_agent/src/business_logics/models/zone_load_summary_model.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:national_life_insurance_agent/src/business_logics/providers/agent_dashboard_provider.dart';
import 'package:national_life_insurance_agent/src/business_logics/providers/personal_details_provider.dart';
import 'package:national_life_insurance_agent/src/views/utils/colors.dart';
import 'package:national_life_insurance_agent/src/views/widgets/profile_avatar.dart';
import '../../business_logics/models/load_summary_model.dart';



class AgentDashboard extends StatefulWidget {
  const AgentDashboard({Key? key}) : super(key: key);

  @override
  State<AgentDashboard> createState() => _AgentDashboardState();
}

class _AgentDashboardState extends State<AgentDashboard> {

  String selectedYearType = "Current Year";
  String selectedPGType = "Premium";

  int selectedIndex = 0;
  List<bool> isSelected = [true, false, false, false];
  TextStyle titleStyle = const TextStyle(
      fontWeight: FontWeight.w400, color: kWhiteColor, fontSize: 12);
  TextStyle bodyStyle = const TextStyle(
      fontSize: 16, fontWeight: FontWeight.bold, color: kWhiteColor);
  List<String> buttonList = [];
  ZoneOrBranch? selectedZone;
  List<ZoneOrBranch> zones = [];
  bool isTrue = false;

  Map<String, Map<String, List<Branch>>> zoneGroupedData = {};
  Map<String, double> zoneFrTotalsByYear = {};
  Map<String, double> zoneRrTotalsByYear = {};


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<AgentDashboardProvider>(context, listen: false).loadSummary();
      Provider.of<AgentDashboardProvider>(context, listen: false).getBannersData();
    });
  }


  void _zoneWiseData(AgentDashboardProvider agentDashboardProvider) {
    agentDashboardProvider.zoneLoadSummary(
      designation: UserData.designation.toString(),
      zoneCode: selectedZone!.zbCode.toString(),
    );
  }


  @override
  Widget build(BuildContext context) {
    final loadSummaryModel = Provider.of<AgentDashboardProvider>(context).loadSummaryModel;
    var zoneLoadSummeryModel = Provider.of<AgentDashboardProvider>(context, listen: false).ZoneLoadSummeryModel;

    //.........................................Without Zone.......................................................
    Map<String, List<Business>>? projectData = loadSummaryModel.business?.fold<
        Map<String, List<Business>>?>(
      null,
          (Map<String, List<Business>>? map, Business data) {
        map ??= {};
        if (!map.containsKey(data.project)) {
          map[data.project.toString()] = [];
        }
        map[data.project]!.add(data);
        return map;
      },
    );

    Map<String, double> frTotalsByYear = {};
    Map<String, double> rrTotalsByYear = {};
    if (projectData != null) {
      calculateTotalsByYear(projectData, "FR", frTotalsByYear);
      calculateTotalsByYear(projectData, "RR", rrTotalsByYear);
    }

    Map<String, Map<String, List<Business>>> groupedData = {};
    if (loadSummaryModel.business != null) {
      for (Business data in loadSummaryModel.business!) {
        String year = data.year ?? 'N/A';
        String projectName = data.project ?? 'N/A';

        if (!groupedData.containsKey(year)) {
          groupedData[year] = {};
        }
        if (!groupedData[year]!.containsKey(projectName)) {
          groupedData[year]![projectName] = [];
        }
        groupedData[year]![projectName]!.add(data);
      }
    }


    double calculateGrowthRate(double previousValue, double currentValue) {
      if (previousValue == 0) {
        return 0; // Avoid division by zero
      }
      return ((previousValue - currentValue) / currentValue) * 100;
    }


    //.........................................With Zone.......................................................

    zones = loadSummaryModel.zoneOrBranch ?? [];


    Map<String, List<Branch>>? zoneProjectData = zoneLoadSummeryModel.branch
        ?.fold<Map<String, List<Branch>>?>(
      null,
          (Map<String, List<Branch>>? map, Branch data) {
        map ??= {};
        if (!map.containsKey(data.project)) {
          map[data.project.toString()] = [];
        }
        map[data.project]!.add(data);
        return map;
      },
    );

    Map<String, double> zoneFrTotalsByYear = {};
    Map<String, double> zoneRrTotalsByYear = {};
    if (zoneProjectData != null) {
      zoneCalculateTotalsByYear(zoneProjectData, "FR", zoneFrTotalsByYear);
      zoneCalculateTotalsByYear(zoneProjectData, "RR", zoneRrTotalsByYear);
    }

    Map<String, Map<String, List<Branch>>> zoneGroupedData = {};
    if (zoneLoadSummeryModel.branch != null) {
      zoneGroupedData = {};
      for (Branch data in zoneLoadSummeryModel.branch!) {
        String year = data.year ?? 'N/A';
        String zoneProjectName = data.project ?? 'N/A';

        if (!zoneGroupedData.containsKey(year)) {
          zoneGroupedData[year] = {};
        }
        if (!zoneGroupedData[year]!.containsKey(zoneProjectName)) {
          zoneGroupedData[year]![zoneProjectName] = [];
        }
        zoneGroupedData[year]![zoneProjectName]!.add(data);
      }
    }


    return Scaffold(
      // drawer: const MainSideBar(),
      appBar: AppBar(
          elevation: 0,
          centerTitle: false,
          toolbarHeight: 65,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Welcome Back',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.4,
                    fontSize: 12),
              ),
              const SizedBox(height: 2),
              Consumer<PersonalDetailsProvider>(
                builder: (context, nameProvider, child) =>
                nameProvider.personalDetailsInProgress
                    ? const Text(
                  'Home',
                  maxLines: 1,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                )
                    : Text(
                  UserData.userName ?? 'N/A',
                  maxLines: 1,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          actions: [
            Consumer<PersonalDetailsProvider>(
              builder: (context, imageProvider, child) =>
              imageProvider.personalDetailsInProgress
                  ? const Padding(
                padding: EdgeInsets.all(8.0),
                child: ProfileAvatarWidget(
                  imageType: ImageType.Asset,
                  url: '',
                  radius: 40,
                  userName: 'U',
                ),
              )
                  : Padding(
                padding: const EdgeInsets.all(8.0),
                child: ProfileAvatarWidget(
                  imageType: ImageType.Network,
                  url: imageProvider.personalDetailsModel.data?.first
                      .agentImage ??
                      '',
                  radius: 50,
                  userName: imageProvider.personalDetailsModel.data
                      ?.first.agentName ??
                      "${UserData.userName}",
                ),
              ),
            )
          ]),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [


            const BranchAndAgentDetailsCard(),
            Card(
              color: kThemeColor,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5),
                    bottomRight: Radius.circular(5),
                  )),
              margin: EdgeInsets.zero,
              child: Column(
                children: [
                  const SizedBox(height: 8),
                  Consumer<AgentDashboardProvider>(
                      builder: (context, agentDashboardProvider, child) {
                        return agentDashboardProvider.getBannersInProgress
                            ? SizedBox(
                          height: 120,
                          child: Scrollbar(
                            child: ListView.builder(
                                itemCount: 5,
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return Shimmer(
                                    interval: const Duration(
                                        milliseconds: 100),
                                    child: Container(
                                      height: 160,
                                      width: 280,
                                      color: Colors.grey[300],
                                      margin: const EdgeInsets.all(8),
                                    ),
                                  );
                                }),
                          ),
                        )
                            : (agentDashboardProvider.bannerModel.data
                            ?.length ?? 0) > 0
                            ? Consumer<AgentDashboardProvider>(builder:
                            (context, agentDashboardProvider, child) {
                          return agentDashboardProvider
                              .getBannersInProgress
                              ? SizedBox(
                            height: 120,
                            child: Scrollbar(
                              child: ListView.builder(
                                  itemCount: 5,
                                  scrollDirection:
                                  Axis.horizontal,
                                  shrinkWrap: true,
                                  itemBuilder:
                                      (context, index) {
                                    return Shimmer(
                                      interval:
                                      const Duration(
                                          milliseconds:
                                          100),
                                      child: Container(
                                        height: 160,
                                        width: 280,
                                        color:
                                        Colors.grey[300],
                                        margin:
                                        const EdgeInsets
                                            .all(8),
                                      ),
                                    );
                                  }),
                            ),
                          )
                              : (agentDashboardProvider.bannerModel.data
                              ?.length ?? 0) > 0
                              ? CarouselSlider(
                            options: CarouselOptions(
                                height: 120.0,
                                autoPlay: true,
                                enlargeCenterPage: true,
                                enlargeStrategy:
                                CenterPageEnlargeStrategy.height),
                            items: agentDashboardProvider.bannerModel.data!
                                .map((banner) {
                              return Builder(
                                builder: (BuildContext
                                context) {
                                  return Container(
                                    height: 120,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 4),
                                    width: MediaQuery
                                        .of(context)
                                        .size
                                        .width,
                                    decoration:
                                    BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(banner.image!),
                                          fit: BoxFit.fitWidth),
                                      borderRadius:
                                      BorderRadius.circular(20.0),
                                    ),
                                  );
                                },
                              );
                            }).toList(),
                          )
                              : const SizedBox();
                        })
                            : const SizedBox();
                      }),
                  const SizedBox(height: 16)
                ],
              ),
            ),

            SizedBox(height: 2.h),
            Container(
              height: 6.h,
              decoration: BoxDecoration(
                  color: Color(0xFFF5F5F5)
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 20, vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedYearType = "Current Year";
                        });
                      },
                      child: Container(
                        height: 6.h,
                        width: 30.w,
                        decoration: BoxDecoration(
                          color: selectedYearType == "Current Year"
                              ? Color(0xFF3F27B4)
                              : Color(0xFFF5F5F5),
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Center(
                          child: Text(
                            "Current Year",
                            style: TextStyle(
                              color: selectedYearType == "Current Year"
                                  ? Colors.white
                                  : Color(0xFF3F27B4),
                              fontWeight: FontWeight.w400,
                              fontSize: 14.px,
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedYearType = "Previous Year";
                        });
                      },
                      child: Container(
                        height: 6.h,
                        width: 30.w,
                        decoration: BoxDecoration(
                          color: selectedYearType == "Previous Year"
                              ? Color(0xFF3F27B4)
                              : Color(0xFFF5F5F5),
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Center(
                          child: Text(
                            "Previous Year",
                            style: TextStyle(
                              color: selectedYearType == "Previous Year"
                                  ? Colors.white
                                  : Color(0xFF3F27B4),
                              fontWeight: FontWeight.w400,
                              fontSize: 14.px,
                            ),
                          ),
                        ),
                      ),
                    ),

                  ],
                ),

              ),
            ),

            SizedBox(
              child: ListView.builder(
                  itemCount: 1,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    String year = selectedYearType == "Current Year"
                        ? DateTime.now().year.toString()
                        : (DateTime.now().year - 1).toString();
                    if (!groupedData.containsKey(year)) {
                      return const SizedBox();
                    }

                    Map<String, List<Business>>? projectData = groupedData[year];
                    if (projectData == null) {
                      return const SizedBox();
                    }


                    // Map<String,
                    //     List<Business>> projectData = groupedData[year]!;
                    double frTotal = frTotalsByYear[year] ?? 0;
                    double rrTotal = rrTotalsByYear[year] ?? 0;

                    double previousFRValue = 0;
                    double previousRRValue = 0;

                    if (frTotalsByYear.containsKey(
                        (int.parse(year) - 1).toString())) {
                      previousFRValue =
                          frTotalsByYear[(int.parse(year) - 1).toString()] ?? 0;
                    }

                    if (rrTotalsByYear.containsKey(
                        (int.parse(year) - 1).toString())) {
                      previousRRValue =
                          rrTotalsByYear[(int.parse(year) - 1).toString()] ?? 0;
                    }

                    double growthRateFR =
                    calculateGrowthRate(previousFRValue, frTotal);
                    double growthRateRR =
                    calculateGrowthRate(previousRRValue, rrTotal);


                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 16),
                          Text(
                            "Year: $year",
                            style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF433E5B),
                              fontSize: 18,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 1,
                                  offset: const Offset(
                                      0, 0), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Stack(
                              children: [
                                Container(
                                  // width: 90.w,
                                  height: 5.h,
                                  decoration: BoxDecoration(
                                    color: Color(0xFF002580),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15, right: 15),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .spaceBetween,
                                      children: const [
                                        Text(
                                          "Plan",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFFFCFCFD)
                                          ),
                                        ),
                                        Text(
                                          "First Year",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFFFCFCFD)
                                          ),
                                        ),
                                        Text(
                                          "Renewal",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFFFCFCFD)
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 15,
                                      right: 15,
                                      top: 7.h,
                                      bottom: 15),
                                  child: Column(
                                    children: [
                                      for (String projectName in projectData
                                          .keys)
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .stretch,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment
                                                  .spaceAround,
                                              children: [
                                                SizedBox(
                                                  child: Text(
                                                    projectName,
                                                    style: const TextStyle(
                                                        fontSize: 13,
                                                        fontWeight: FontWeight
                                                            .w400,
                                                        color: Color(0xFF433E5B)
                                                    ),

                                                  ),
                                                ),
                                                SizedBox(width: 4.w),
                                                SizedBox(
                                                  width: 20.w,
                                                  child: Text(
                                                    "${getTotalFR(
                                                        projectData[projectName] ??
                                                            [])}",
                                                    style: const TextStyle(
                                                      fontSize: 13,
                                                      fontWeight: FontWeight
                                                          .w400,
                                                      color: Color(0xFF433E5B),
                                                    ),
                                                  ),
                                                ),

                                                SizedBox(
                                                  width: 20.w,
                                                  child: Text(
                                                    "${getTotalRR(
                                                        projectData[projectName] ??
                                                            [])}",
                                                    style: const TextStyle(
                                                      fontSize: 13,
                                                      fontWeight: FontWeight
                                                          .w400,
                                                      color: Color(0xFF433E5B),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 1.h)

                                          ],
                                        ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          const Text(
                                            "Total",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight
                                                    .w700,
                                                color: Color(0xFF433E5B)
                                            ),
                                          ),
                                          SizedBox(width: 10.w),
                                          SizedBox(
                                            width: 20.w,
                                            child: Text(
                                              "$frTotal",
                                              style: const TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF433E5B),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 20.w,
                                            child: Text(
                                              "$rrTotal",
                                              style: const TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF433E5B),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    );
                  }
              ),
            ),

            ////.....................Zone..................................
            SizedBox(height: 5.h),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: DropdownButtonFormField<ZoneOrBranch>(
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Color(0xFF002580), width: 2),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Color(0xFF002580), width: 2),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  filled: true,
                ),
                hint: Text("Select Zone"),
                borderRadius: BorderRadius.circular(10),
                value: selectedZone,
                onChanged: (ZoneOrBranch? newValue) {
                  if (newValue != null) {
                    setState(() {
                      selectedZone = newValue;
                      _zoneWiseData(AgentDashboardProvider());
                      isTrue = true;
                    });
                  }
                },
                items: zones.map<DropdownMenuItem<ZoneOrBranch>>((zone) {
                  return DropdownMenuItem<ZoneOrBranch>(
                    value: zone,
                    child: Text("${zone.zbName} (${zone.zbCode})" ?? ''),
                  );
                }).toList(),
              ),
            ),

            SizedBox(
                  child: ListView.builder(
                  itemCount: 1,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    String year = selectedYearType == "Current Year"
                        ? DateTime.now().year.toString()
                        : (DateTime.now().year - 1).toString();
                    if (!zoneGroupedData.containsKey(year)) {
                      return const SizedBox(child: Text("No Data"),);
                    }

                    Map<String, List<Branch>>? zoneProjectData = zoneGroupedData[year];
                    if (zoneProjectData == null) {
                      return const SizedBox();
                    }

                   
                    double zoneFrTotal = zoneFrTotalsByYear[year] ?? 0;
                    double zoneRrTotal = zoneRrTotalsByYear[year] ?? 0;

                    double zonePreviousFRValue = 0;
                    double zonePreviousRRValue = 0;

                    if (zoneFrTotalsByYear.containsKey(
                        (int.parse(year) - 1).toString())) {
                      zonePreviousFRValue =
                          zoneFrTotalsByYear[(int.parse(year) - 1).toString()] ?? 0;
                    }

                    if (zoneRrTotalsByYear.containsKey(
                        (int.parse(year) - 1).toString())) {
                      zonePreviousRRValue =
                          zoneRrTotalsByYear[(int.parse(year) - 1).toString()] ?? 0;
                    }

                    double growthRateFR =
                    calculateGrowthRate(zonePreviousFRValue, zoneFrTotal);
                    double growthRateRR =
                    calculateGrowthRate(zonePreviousRRValue, zoneRrTotal);


                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 16),
                          Text(
                            "Year: $year",
                            style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF433E5B),
                              fontSize: 18,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 1,
                                  offset: const Offset(
                                      0, 0), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Stack(
                              children: [
                                Container(
                                  // width: 90.w,
                                  height: 5.h,
                                  decoration: BoxDecoration(
                                    color: Color(0xFF002580),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15, right: 15),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .spaceBetween,
                                      children: [
                                        Text(
                                          "Plan",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFFFCFCFD)
                                          ),
                                        ),
                                        Text(
                                          "First Year",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFFFCFCFD)
                                          ),
                                        ),
                                        Text(
                                          "Renewal",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFFFCFCFD)
                                          ),
                                        ),
                                        // Text(
                                        //   "Singel",
                                        //   style: TextStyle(
                                        //       fontSize: 15,
                                        //       fontWeight: FontWeight.w500,
                                        //       color: Color(0xFFFCFCFD)
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 15,
                                      right: 15,
                                      top: 7.h,
                                      bottom: 15),
                                  child: Column(
                                    children: [
                                      for (String projectName in zoneProjectData
                                          .keys)
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              children: [
                                                SizedBox(
                                                  child: Text(
                                                    projectName,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight: FontWeight
                                                            .w400,
                                                        color: Color(0xFF433E5B)
                                                    ),

                                                  ),
                                                ),
                                                SizedBox(width: 4.w),
                                                SizedBox(
                                                  width: 20.w,
                                                  child: Text(
                                                    "${zoneGetTotalFR(zoneProjectData[projectName] ?? [])}",
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontWeight: FontWeight.w400,
                                                      color: Color(0xFF433E5B),
                                                    ),
                                                  ),
                                                ),

                                                SizedBox(
                                                  width: 20.w,
                                                  child: Text(
                                                    "${zoneGetTotalRR(zoneProjectData[projectName] ?? [])}",
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontWeight: FontWeight.w400,
                                                      color: Color(0xFF433E5B),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 1.h)

                                          ],
                                        ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceAround,
                                        children: [

                                          Text(
                                            "Total",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight
                                                    .w700,
                                                color: Color(0xFF433E5B)
                                            ),
                                          ),
                                          SizedBox(width: 5.w),
                                          SizedBox(
                                            width: 20.w,
                                            child: Text(
                                              "$zoneFrTotal",
                                              style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF433E5B),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 20.w,
                                            child: Text(
                                              "$zoneRrTotal",
                                              style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF433E5B),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    );
                  }
              ),
            ),

            const SizedBox(height: 16),
          ],
        ),
      ),
    );
  }

//..............Without Zone...................
  double getTotalFR(List<Business> dataList) {
    double totalFR = 0;
    for (Business data in dataList) {
      if (data.type == "FR") {
        double premiumPaid = double.tryParse(
            data.totalPremiumPaidInLacs ?? "0") ?? 0;

        totalFR += premiumPaid;
      }
    }
    return totalFR;
  }

  double getTotalRR(List<Business> dataList) {
    double totalRR = 0;
    for (Business data in dataList) {
      if (data.type == "RR") {
        double premiumPaid = double.tryParse(
            data.totalPremiumPaidInLacs ?? "0") ?? 0;
        totalRR += premiumPaid;
      }
    }
    return totalRR;
  }

  void calculateTotalsByYear(Map<String, List<Business>> projectData,
      String type, Map<String, double> totalsByYear) {
    projectData.forEach((projectName, dataList) {
      dataList.forEach((data) {
        if (data.type == type) {
          String year = data.year ?? "";
          double total = totalsByYear[year] ?? 0;
          total += double.tryParse(data.totalPremiumPaidInLacs ?? "0") ?? 0;
          totalsByYear[year] = total;
        }
      });
    });
  }

  //......................With Zone.................
  double zoneGetTotalFR(List<Branch> zoneDataList) {
    double totalFR = 0;
    for (Branch data in zoneDataList) {
      if (data.type == "FR") {
        double premiumPaid = double.tryParse(
            data.totalPremiumPaidInLacs ?? "0") ?? 0;

        totalFR += premiumPaid;
      }
    }
    return totalFR;
  }

  double zoneGetTotalRR(List<Branch> zoneDataList) {
    double totalRR = 0;
    for (Branch data in zoneDataList) {
      if (data.type == "RR") {
        double premiumPaid = double.tryParse(
            data.totalPremiumPaidInLacs ?? "0") ?? 0;
        totalRR += premiumPaid;
      }
    }
    return totalRR;
  }

  void zoneCalculateTotalsByYear(Map<String, List<Branch>> zoneProjectData,
      String type, Map<String, double> totalsByYear) {
    zoneProjectData.forEach((projectName, dataList) {
      dataList.forEach((data) {
        if (data.type == type) {
          String year = data.year ?? "";
          double total = totalsByYear[year] ?? 0;
          total += double.tryParse(data.totalPremiumPaidInLacs ?? "0") ?? 0;
          totalsByYear[year] = total;
        }
      });
    });
  }
}


class BranchAndAgentDetailsCard extends StatelessWidget {
  const BranchAndAgentDetailsCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kThemeColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
        child: Consumer<PersonalDetailsProvider>(
          builder: (context, infoProvider, child) =>
              infoProvider.personalDetailsInProgress
                  ? const Center(
                      child: CircularProgressIndicator(
                      color: kWhiteColor,
                    ))
                  : Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Designation',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 11,
                                    color: kWhiteColor),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Text(
                                UserData.designation ?? 'N/A',
                                maxLines: 2,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kWhiteColor),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Employee ID',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 11,
                                    color: kWhiteColor),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Text(
                                UserData.employeeID ?? 'N/A',
                                maxLines: 2,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kWhiteColor),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
        ),
      ),
    );
  }
}



